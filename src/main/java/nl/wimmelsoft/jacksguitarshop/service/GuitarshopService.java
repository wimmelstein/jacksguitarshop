package nl.wimmelsoft.jacksguitarshop.service;

import nl.wimmelsoft.jacksguitarshop.model.Guitar;
import nl.wimmelsoft.jacksguitarshop.repository.GuitarRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GuitarshopService {

    private GuitarRepository repository;

    public GuitarshopService(GuitarRepository repository) {
        this.repository = repository;
    }

    public List<Guitar> listAllGuitars() {
        return (List<Guitar>) repository.findAll();
    }

    public Guitar findGuitarById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public void saveGuitar(Guitar guitar) {
        repository.save(guitar);
    }

    public void deleteGuitarById(long id) {
        repository.deleteById(id);
    }
}
