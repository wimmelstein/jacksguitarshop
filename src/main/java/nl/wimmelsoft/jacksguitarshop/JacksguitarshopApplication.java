package nl.wimmelsoft.jacksguitarshop;

import nl.wimmelsoft.jacksguitarshop.model.Guitar;
import nl.wimmelsoft.jacksguitarshop.model.Type;
import nl.wimmelsoft.jacksguitarshop.repository.GuitarRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class JacksguitarshopApplication{

	public static void main(String[] args) {
		SpringApplication.run(JacksguitarshopApplication.class, args);
	}


	@Bean
	ApplicationRunner runner(GuitarRepository repository) {

		return args -> {
			Arrays.asList(
					new Guitar("Fender", "Telecaster", Type.Electric),
					new Guitar("Fender", "Stratocaster", Type.Electric),
					new Guitar("Gibson", "Les Paul", Type.Electric),
					new Guitar("Martin", "D100", Type.Accoustic)
			).forEach(guitar -> repository.save(guitar));

			repository.findAll().forEach(System.out::println);
		};
	}
}
