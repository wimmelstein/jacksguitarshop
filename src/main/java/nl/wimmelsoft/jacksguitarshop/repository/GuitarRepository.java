package nl.wimmelsoft.jacksguitarshop.repository;

import nl.wimmelsoft.jacksguitarshop.model.Guitar;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuitarRepository extends CrudRepository<Guitar, Long> {

}
