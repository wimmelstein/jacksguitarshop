package nl.wimmelsoft.jacksguitarshop.controller;

import nl.wimmelsoft.jacksguitarshop.model.Guitar;
import nl.wimmelsoft.jacksguitarshop.service.GuitarshopService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/guitars")
public class GuitarController {

    private GuitarshopService service;

    public GuitarController(GuitarshopService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET )
    public List<Guitar> getAllGuitars() {
        return service.listAllGuitars();
    }

    @GetMapping(value = "/{id}")
    public Guitar getGuitarById(@PathVariable long id) {
        return service.findGuitarById(id);
    }

    @PostMapping
    public void saveGuitar(@RequestBody Guitar guitar) {
        service.saveGuitar(guitar);
    }

    @PutMapping(value = "/{id}")
    public void updateGuitarById(@RequestBody Guitar guitar) {
        service.saveGuitar(guitar);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteGuitarById(@PathVariable long id) {
        service.deleteGuitarById(id);
    }

}
